from src.interprete.narrador import Locutor

import unittest


class TestLocutor(unittest.TestCase):

    #  generates a narrative of the fight
    #  formats the narrative with player names, moves, and attacks
    def test_formats_narrative(self):
        locutor = Locutor(default_names={"player1": "Tony", "player2": "Arnaldor"})
        locutor.players = {
            "player1": {
                "movimientos": ["A", "S", "D"],
                "golpes": ["P", "K"]
            },
            "player2": {
                "movimientos": ["W", "S", "D"],
                "golpes": ["P", "K"]
            }
        }
        formatted_narrative = locutor._formatter('player1', 'ASDP', 'player2')
        expected_narrative = "Tony retrocede, agacha, avanza y le da un puñetazo al pobre de Arnaldor"
        self.assertEqual(formatted_narrative, expected_narrative)

    #  extracts the movement and name of the attack from the player
    def test_extracts_movement_and_name(self):
        locutor = Locutor()
        movement, name = locutor._extract_attack('player1', 'ASDP')
        expected_movement = 'ASD'
        expected_name = 'Puño'
        self.assertEqual(movement, expected_movement)
        self.assertEqual(name, expected_name)

    #  returns 0 if no attack is found
    def test_returns_zero_if_no_attack(self):
        locutor = Locutor()
        attack = locutor._extract_attack('player1', 'XYZ')
        self.assertEqual(attack, ('XYZ', ''))

    #  returns an empty string if no name is found
    def test_returns_empty_string_if_no_name(self):
        locutor = Locutor()
        move, name = locutor._extract_attack('player1', 'XYZ')
        self.assertEqual(move, 'XYZ')
        self.assertEqual(name, '')
