﻿**¡Hola Querid@ Candidat@!**

A continuación, te presentamos el siguiente desafío para postular al puesto de desarrollador@ Software Developer
Engineer.
Te invitamos a hacer tu mejor esfuerzo, no importa si no terminas y solo lo resuelves parcialmente, desde ya
te agradecemos por participar.

**Evaluación:**
No solo evaluamos algoritmos, también buenas prácticas de arquitectura, limpieza de código, estructura
del proyecto, y en general cómo dejas tu marca de seniority en la entrega, por lo que te recomendamos que hagas tu mejor
esfuerzo.

**Duración y en que consta la prueba:**

- 48 horas Si terminas antes mejor, pero si necesita más tiempo, dale, nos enfocamos más en una prueba orientada a
  resultados a que solo medir el tiempo)
- Deberás desarrollar una solución que relate la pelea y diga el resultado final.
- Deberás responder unas preguntas para conocerte un poco más

**Forma de entrega:**
El proyecto deberá estar en un repositorio compartido. Si lo entregas en docker mucho mejor.

**Lenguajes, frameworks y herramientas:**
Usa lenguaje que más te acomode Si usas Python, mucho mejor, también nos
gusta Go)

**Ejercicio**

**Talana Kombat JRPG**

Talana Kombat es un juego donde 2 personajes se enfrentan hasta la muerte.
Cada personaje tiene 2 golpes especiales que se ejecutan con una combinación de movimientos + 1 botón de golpe.

**Los botones que se usan son**

<table>
    <tr>
        <td rowspan="2">
            <div style="text-align: center;"><img src="docs/img/teclas.jpg" width="640"  alt=""></div>
        </td>
        <td> (W) -> Arriba, <br>(S) -> Abajo, <br>(A) -> Izquierda, <br>(D) -> Derecha </td>
    </tr>
    <tr>
        <td>P 
            <span>-> Puño</span> <br>
            <span>K-> Patada</span> 
        </td>
    </tr>
</table>

**Golpes de nuestros personajes**
![](docs/img/golpes.png)

![](docs/img/golpes2.png)

**Información importante:**

Parte atacando el jugador que envió una combinación menor de botones (movimiento + golpes), en caso de empate, parte él
con menos movimientos, si empatan de nuevo, inicia él con menos golpes, si hay empate de nuevo, inicia el player 1 (total el player 2 siempre es del hermano chico)

La secuencia completa del combate de cada jugador se entrega de una vez (consolidada en un json)

Cada personaje tiene 6 Puntos de energía

- Un personaje muere cuando su energía llega a 0 y de inmediato finaliza la pelea
- Tony es el player 1, siempre ataca hacia la derecha (y no cambia de lado)
- Arnaldor es el player 2, siempre ataca hacia la izquierda (y no cambia de lado)
- Los personajes se atacan uno a la vez estilo JRPG, por turnos hasta que uno es derrotado, los golpes no pueden ser
  bloqueados, se asume que siempre son efectivos.

Los datos llegan como un json con botones de movimiento y golpe que se correlacionan para cada jugada

Los movimientos pueden ser un string de largo máximo 5 (puede ser vacío) Los golpes pueden ser un solo botón maximo (puede ser vacío)

Se asume que el botón de golpe es justo después de la secuencia de movimiento, es decir, AADSD + P es un Taladoken (antes se movió para atrás 2 veces); DSDAA + P son movimientos más un puño

**Para este desafío:**
Desarrolla una solución que relata la pelea e informe el resultado final

Por ejemplo para el siguiente json de pelea:

```json
{
  "player1": {
    "movimientos": [
      "D",
      "DSD",
      "S",
      "DSD",
      "SD"
    ],
    "golpes": [
      "K",
      "P",
      "",
      "K",
      "P"
    ]
  },
  "player2": {
    "movimientos": [
      "SA",
      "SA",
      "SA",
      "ASA",
      "SA"
    ],
    "golpes": [
      "K",
      "",
      "K",
      "P",
      "P"
    ]
  }
}
```

- Tonyn avanza y da una patada
- Arnaldor conecta un Remuyuken
- Tonyn usa un Taladoken
- Arnaldor se mueve
- Tonyn le da un puñetazo al pobre Arnaldor
- Arnaldor conecta un Remuyuken
- Arnardor Gana la pelea y aún le queda 1 de energía

**Otros ejemplos:**

Gana Tonyn

```json
{
  "player1": {
    "movimientos": [
      "SDD",
      "",
      "DSD",
      "",
      "SA",
      "DSD"
    ],
    "golpes": [
      "K",
      "P",
      "K",
      "P"
    ]
  },
  "player2": {
    "movimientos": [
      "DSD",
      "WSAW",
      "ASA",
      "",
      "ASA",
      "SA"
    ],
    "golpes": [
      "P",
      "K",
      "K",
      "K",
      "P",
      "k"
    ]
  }
}
```

Gana Arnaldor

```json
{
  "player1": {
    "movimientos": [
      "DSD",
      "S"
    ],
    "golpes": [
      "P",
      ""
    ]
  },
  "player2": {
    "movimientos": [
      "",
      "ASA",
      "DA",
      "AAA",
      "",
      "SA"
    ],
    "golpes": [
      "P",
      "",
      "P",
      "K",
      "K",
      "K"
    ]
  }
}
```

**Preguntas generales**

1. Supongamos que en un repositorio GIT hiciste un commit y olvidaste un archivo. Explica cómo se soluciona si hiciste
   push, y cómo si aún no hiciste.

   De ser posible, que quede solo un commit con los cambios.

```shell
git add [file]

git commit --amend -m "[Nuevo mensaje del Commit]" | git commit --amend --no-edit

git push | git push --force
```

2. Si has trabajado con control de versiones ¿Cuáles han sido los flujos con los que has trabajado?

Regularmente, se trabaja con el flujo de Ramas por Caracteristicas o Issues y/o Repositorio Centralizado.
También se ha trabajado con Flujo de Entrega continua o en muchas ocaciones una mezcla de los varios flujos con reglas
de fusion

3. ¿Cuál ha sido la situación más compleja que has tenido con esto?

Definicion de prioridades en el momento de despliegue para realizar el versionamiento a las ramas principales, ya que
esto
conlleva conflicto cuando se tiene diferentes tareas que intervienen 2 o más componentes similares

4. ¿Qué experiencia has tenido con los microservicios?

He desarrollado Microservicios y he dado mantenimiento a otros en lenguajes como python y java con integracion continua
y despliegue contiuo a la nube de openshift a travez de los runners de gitlab

5. ¿Cuál es tu servicio favorito de GCP o AWS? ¿Por qué?

Ambos son excelentes servicios y dan mucha variedad en su plataforma para implementar varias soluciones
no tendria preferencia en alguna pero por desglose de servicios eligiria AWS


**¡Te deseamos mucha suerte!**
