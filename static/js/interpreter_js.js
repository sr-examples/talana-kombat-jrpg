/**
 * validateJSON es una función que se utiliza para validar si la entrada proporcionada en el campo 'jsonInput' es un
 * JSON válido o no.
 *
 * Si la entrada es un JSON válido:
 *   1. Muestra una alerta que dice 'Formato JSON válido.'
 *   2. Habilita el botón de envío ('sendButton').
 *   3. Limpia las áreas de respuesta y error anteriores utilizando la función clearResponseAndErrorAreas.
 *   4. Muestra la respuesta del servidor con animación de escritura por línea utilizando la función
 *   showResponseWithAnimation.
 *
 * En caso de que la entrada no sea un JSON válido, captura el error y muestra una alerta que dice 'Error:
 * Formato JSON no válido.'.
 * No devuelve nada.
 */
function validateJSON() {
    const jsonInput = document.getElementById('jsonInput');

    if (!jsonInput || !jsonInput.value) {
        alert('Error: Input vacío.');
        return;
    }

    try {
        const parsedJSON = JSON.parse(jsonInput.value);
        alert('Formato JSON válido.');
        clearResponseAndErrorAreas();
        showResponseWithAnimation(parsedJSON);
    } catch (error) {
        alert('Error: Formato JSON no válido.');
    }
}

/**
 * sendToAPI es una función JavaScript que envía una solicitud HTTP POST a la API '/interprete' con un JSON.
 * El JSON debe estar en el campo de entrada 'jsonInput'.
 *
 * Si la respuesta HTTP es exitosa:
 * - Muestra un mensaje de alerta indicando que la respuesta fue obtenida correctamente.
 * - Limpia el área de error previo (con la función clearErrorArea).
 * - Muestra la respuesta del servidor con la función showResponseWithAnimation.
 *
 * Si la solicitud no fue exitosa:
 * - Muestra un mensaje de alerta indicando que hubo un error al enviar el JSON.
 * - Muestra un mensaje de error en el área de error con la función showError.
 */
function sendToAPI() {
    const jsonInput = document.getElementById('jsonInput').value;

    fetch('/interpreter', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonInput,
    })
        .then(response => response.json())
        .then(data => {
            console.log('Respuesta de la API:', data);
            alert('Respuesta obtenida correctamente de la API.');

            // Limpiar área de error anterior
            clearErrorArea();

            // Mostrar respuesta del servidor con animación de escritura por línea
            showResponseWithAnimation(data);
        })
        .catch(error => {
            console.error('Error al enviar JSON a la API:', error);
            alert('Error al enviar JSON a la API.');

            // Mostrar mensaje de error en el área de error
            showError('No se ha podido obtener respuesta para realizar la narración.');
        });
}

/**
 * La función showResponseWithAnimation es una función que despliega una respuesta con animación.
 * La respuesta se despliega una línea a la vez en el área de respuesta ('responseArea').
 *
 * @param {Object} response - El objeto de respuesta para mostrar.
 *
 * La función realiza las siguientes operaciones:
 *  1. Obtiene referencias al elemento del DOM 'responseArea' y limpia el contenido existente.
 *  2. Obtiene las claves del objeto de respuesta.
 *  3. Inicializa un índice.
 *  4. Declara la función appendLine que añade una línea de respuesta cada vez que es invocada.
 *      - Si el índice es menor que la cantidad de claves, añade la siguiente línea de respuesta al 'responseArea',
 *        incrementa el índice y programa la siguiente invocación de appendLine en 250 ms.
 *  5. Invoca por primera vez a appendLine para iniciar el proceso descriptivo en el punto 4.
 *
 * No retorna ningún valor.
 */
function showResponseWithAnimation(response) {
    if (typeof response !== 'object' || response === null) {
        console.error('Invalid argument passed to showResponseWithAnimation. Expected an object.');
        return;
    }

    const responseArea = document.getElementById('responseArea');

    if (!responseArea) {
        console.error('Error: No se encontró el área de respuesta.');
        return;
    }

    responseArea.innerHTML = '';

    function appendLine() {
        if (currentIndex < keys.length) {
            const key = keys[currentIndex];
            responseArea.innerHTML += '>> ' + response[key] + '\n';
            currentIndex++;
            setTimeout(appendLine, 250); // Ajusta el intervalo de tiempo según tu preferencia
        }
    }

    appendLine()
}

/**
 * Esta función clearResponseAndErrorAreas se encarga de limpiar las áreas de respuesta y error en la página web.
 * Utiliza el método getElementById de document para obtener los elementos con los ID 'responseArea' y 'errorArea'.
 * Luego, establece el atributo innerHTML del elemento 'responseArea' y el atributo value del elemento 'errorArea' a
 * cadenas vacías.
 */
function clearResponseAndErrorAreas() {
    const responseArea = document.getElementById('responseArea');
    const errorArea = document.getElementById('errorArea');

    if (responseArea) {
        responseArea.innerHTML = '';
    }

    if (errorArea) {
        errorArea.value = '';
    }
}

/**
 * Esta función clearErrorArea se encarga de limpiar solo el área de error en la página web.
 * Utiliza el método getElementById de document para obtener el elemento con el ID 'errorArea'.
 * Luego, establece el atributo value de este elemento a una cadena vacía.
 */
function clearErrorArea() {
    document.getElementById('errorArea').value = '';
}

/**
 * Esta función showError recibe un mensaje de error como argumento y muestra este error en el área de error en la
 * página web.
 * Utiliza el método getElementById de document para obtener el elemento con el ID 'errorArea'.
 * Luego, establece el atributo value de este elemento al mensaje de error recibido.
 *
 * @param {string} errorMessage - El mensaje de error que se va a mostrar.
 */
function showError(errorMessage) {
    document.getElementById('errorArea').value = errorMessage;
}
