import argparse
import os

import uvicorn

from src.service import app
from src.console import run


def main():
    """
    Función principal encargada de manejar los argumentos de ejecución del programa y decidir el modo de ejecución.

    El argumento del modo de ejecución "--mode" es requerido.
    Los posibles valores para este argumento son "console" y
    "service", donde "console" corresponde al modo de ejecución de los juegos de consola y "service" corresponde al modo
    de ejecución del servidor FastAPI.

    En el modo 'console', ejecuta el juego de consola imprimiendo en pantalla "Ejecutando juego en modo consola..." y
    llamando a la función run().

    En el modo 'service', levanta el servidor FastAPI, imprimiendo en pantalla "Levantando servidor FastAPI...".
    Además, chequea la variable de entorno 'DEBUG'.
    Si es True, imprime "Modo de Ejecucion DESARROLLO" y corre el
    servidor FastAPI en modo de recarga (reload) en "localhost" y puerto 8000.
    Si es False, imprime "Modo de Ejecucion
    PRODUCCION" y corre el servidor FastAPI en el host "0.0.0.0" y puerto 8000.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", type=str, choices=["console", "service"], required=True,
                        help="Ejecuta el programa en modo consola o servicio")

    args = parser.parse_args()

    if args.mode == "console":
        # Aquí es donde se coloca la lógica para ejecutar tu juego en consola
        print("Ejecutando juego en modo consola...")
        run()
    elif args.mode == "service":
        # Esto levantará tu servidor FastAPI
        print("Levantando servidor FastAPI...")
        if os.getenv('DEBUG', 'true') == 'true':
            print('Modo de Ejecucion DESARROLLO')
            uvicorn.run('src.service:app', host="localhost", port=8000, reload=True)
        else:
            print('Modo de Ejecucion PRODUCCION')
            uvicorn.run(app, host="0.0.0.0", port=8000)


if __name__ == "__main__":
    main()
