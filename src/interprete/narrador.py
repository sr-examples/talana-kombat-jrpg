from itertools import zip_longest

from src.game.const import ATAQUES, MOVIMIENTOS
from src.game.get_actions import get_actions
from src.patterns.singlenton import SingletonMeta
from src.utils.effects.escritura import escribir_guion
from src.utils.union_list import union_list


class Locutor(metaclass=SingletonMeta):
    def __init__(self, default_names=None, game: dict = None, players: dict = None):
        """
        Inicializa el Locutor con los nombres por defecto y datos del juego.

        :param default_names: Nombres por defecto de los jugadores.
        :param game: Datos del juego.
        :param players: Datos de los jugadores.
        """
        self.guion = []
        self.default_names = default_names or {"player1": "Tony", "player2": "Arnaldor"}
        self.players = players
        self.game = game

        # if not (isinstance(game, dict) ^ isinstance(players, dict)):
        #     raise ValueError('Solo se debe instanciar 1 bloque de datos')

    @staticmethod
    def _extract_attack(player: str, attack: str) -> tuple:
        """
        Extrae el movimiento y nombre del ataque del jugador.

        :param player: Jugador.
        :param attack: Ataque del jugador.
        :return: Movimiento y nombre del ataque.
        """
        for personal_attack in ATAQUES[player]:
            if personal_attack in attack:
                return attack.replace(personal_attack, ''), ATAQUES[player][personal_attack]['name']
        for default_attack in ATAQUES['default']:
            if default_attack in attack:
                return attack.replace(default_attack, ''), ATAQUES['default'][default_attack]['name']
        return attack, ''

    @staticmethod
    def _extract_move(move):
        """
        Extrae los movimientos del jugador.

        :param move: Movimientos del jugador.
        :return: Lista de movimientos.
        """
        return [MOVIMIENTOS[direction] + (',' if (len(move) - 1) != limit else '') for limit, direction in
                enumerate(move) if direction in MOVIMIENTOS]

    def _formatter(self, player1, attack, player2) -> str:
        """
        Formatea la narrativa del juego.

        :param player1: Jugador 1.
        :param attack: Ataque del jugador.
        :param player2: Jugador 2.
        :return: Narrativa formateada.
        """
        move, damage = self._extract_attack(player1, attack)
        move_actions = self._extract_move(move)

        action = {
            'Remuyuken': 'conecta un Remuyuken',
            'Taladoken': 'usa un Taladoken',
            'Puño': 'le da un puñetazo al pobre de ',
            'Patada': 'le da una patada muy fuerte a '
        }

        text = [self.default_names[player1]]

        if move_actions and damage:
            text.extend(move_actions)
            text.append('y' if len(move_actions) > 1 else 'y')

        if not damage:
            text.append('se mueve')

        else:
            if damage in ["Puño", "Patada"]:
                text.append(f'{action[damage]}{self.default_names[player2]}')
            else:
                text.append(action[damage])

        return ' '.join(text)

    def _get_heath_win(self) -> int:
        """
        Este método calcula la diferencia entre la salud de jugador1 y jugador2 después de la simulación del combate.
        Utiliza la información contenida en los movimientos y ataques de cada jugador para simular el combate.
        En cada turno, el daño de los ataques de los jugadores se resta de la salud del oponente.

        :return: La diferencia absoluta entre la salud de jugador1 y jugador2 al final de la simulación
        """
        # Crear lista de tuplas de ('movimiento', 'golpes')
        combos_player1 = zip(self.players['player1']['movimientos'], self.players['player1']['golpes'])
        combos_player2 = zip(self.players['player2']['movimientos'], self.players['player2']['golpes'])

        # Inicializar la salud de los jugadores
        health1 = 6
        health2 = 6

        if self.players:
            # Simula el combate hasta que uno de los jugadores ya no tenga salud
            while health1 > 0 and health2 > 0:
                for player1, player2 in zip_longest(combos_player1, combos_player2,
                                                    fillvalue=('', '')):
                    damage1 = get_actions('player1', player1, 'point')
                    damage2 = get_actions('player2', player2, 'point')

                    health2 -= damage1
                    health1 -= damage2
        elif self.game:
            pass

        return abs(health1 - health2)

    def crear_guion(self):
        """
        Genera una narrativa de la pelea.
        """
        self.clear_guion()
        if self.players is not None:
            player1_moves = union_list(self.players['player1']['movimientos'], self.players['player1']['golpes'],
                                       formato=False)
            player2_moves = union_list(self.players['player2']['movimientos'], self.players['player2']['golpes'],
                                       formato=False)

            for p1_attack, p2_attack in zip_longest(player1_moves, player2_moves, fillvalue=''):
                if p1_attack:
                    self.guion.append(self._formatter('player1', p1_attack, 'player2'))
                if p2_attack:
                    self.guion.append(self._formatter('player2', p2_attack, 'player1'))

            winner = "player1" if len(player1_moves) > len(player2_moves) else "player2"
            self.guion.append(
                f'{self.default_names[winner]} Gana la pelea y aun le queda {self._get_heath_win()} de salud')

    def narrar(self):
        """
        Narra la pelea generada por 'crear_guion()'.
        """
        self.crear_guion()
        escribir_guion(self.guion, 0.250)

    def clear_guion(self):
        """
        Limpia el guion actual.
        """
        self.guion.clear()
