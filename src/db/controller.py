import json
from typing import Optional

from src.config.settings import BASE_DIR
from src.patterns.singlenton import SingletonMeta


class JsonDB(metaclass=SingletonMeta):
    """
    Maneja una base de datos en formato JSON.

    Atributos:
        data: Contenido de la base de datos.
        filename: Ruta al archivo de la base de datos.
    """

    def __init__(self, filename: Optional[str] = None, default_data: Optional[dict] = None):
        """
        Inicializa la base de datos.

        Args:
            filename (str, optional): Ruta al archivo de la base de datos. Defaults to BASE_DIR / 'db.json'.
            default_data (dict, optional): Estructura de datos por defecto. Defaults a un diccionario predefinido.
        """
        self.data = None
        self.filename = filename or BASE_DIR / 'db.json'
        self.default = default_data or {
            'default': {'player1': 'Tony', 'player2': 'Arnaldor'},
            'last_fight': 1,
            'games': []
        }

        if not self.filename.exists():
            self.save(self.default)

        self.__read()

    def __read(self):
        """Lee el contenido del archivo de la base de datos y lo almacena en data."""
        with open(self.filename, 'r') as file:
            self.data = json.load(file)
        return self.data

    def save(self, data=None):
        """
        Escribe los datos en el archivo de la base de datos.

        Args:
            data (dict, optional): Datos a grabar en el archivo. Defaults a self.data.
        """
        with open(self.filename, 'w') as file:
            json.dump(data or self.data, file, indent=4)
