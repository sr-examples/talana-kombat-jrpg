from src.db.controller import JsonDB
from src.game.const import GOLPES, MOVIMIENTOS
from src.game.get_actions import get_actions
from src.utils.find_game import find_game
from src.utils.keyboard.input_keys import capture_keypress


def save_turn(player: str, fight_id: int, combo: tuple[str, str]) -> None:
    """
    Esta función guarda el turno de un jugador para una pelea en la base de datos.

    Args:
        player (str): El nombre del jugador.
        fight_id (int): El ID de la pelea.
        combo (tuple): El combo de acciones del jugador.

    Returns:
        None
    """
    db = JsonDB()

    # Buscamos la pelea en la base de datos
    fight_exists, index = find_game(db.data['games'], fight_id)

    # Accedemos al diccionario de la pelea
    fight_complete = db.data['games'][index][str(fight_id)]

    # Si la pelea no tiene datos, inicializamos el diccionario de datos del jugador
    if not fight_complete.get('data'):
        fight_complete['data'] = {player: {'movimientos': [], 'golpes': []}}
    # Si el jugador no tiene datos, inicializamos su diccionario de datos
    elif not fight_complete['data'].get(player):
        fight_complete['data'][player] = {'movimientos': [], 'golpes': []}

    # Agregamos los movimientos y golpes del turno al diccionario de datos del jugador
    fight_complete['data'][player]['movimientos'].append(combo[0])
    fight_complete['data'][player]['golpes'].append(combo[1])


# noinspection GrazieInspection
def fight(fight_id: int, player1: str, player2: str, health1: int, health2: int, turn: int = 1) -> dict:
    """
    La función 'fight' controla el ciclo de un combate entre dos jugadores.
    :args
        fight_id (int): ID del combate
        player1 (str): Jugador 1 en el combate
        player2 (str): Jugador 2 en el combate
        health1 (int): Salud del Jugador 1
        health2 (int): Salud del Jugador 2
        turn (int):Turno actual

    :returns
    - Un diccionario con la salud restante de cada jugador
    """
    current_player, other_player = (player1, player2) if turn == 1 else (player2, player1)

    combo = capture_keypress(current_player, list(MOVIMIENTOS.keys()), GOLPES,
                             instructions=(health1 == 6 and health2 == 6))

    save_turn(f'player{turn}', fight_id, combo)

    damage = get_actions(f'player{turn}', combo, 'point')
    print(other_player, f'recibe {damage} de daño')

    if turn == 1:
        health2 -= damage
    else:
        health1 -= damage

    print(f'Salud restante {health2 if turn == 1 else health1}')

    if health1 >= 1 and health2 >= 1:
        return fight(fight_id, player1, player2, health1, health2, 2 if turn == 1 else 1)

    return {'player1': health1, 'player2': health2}
