from time import sleep

from src.db.controller import JsonDB
from src.game.fight import fight
from src.game.menus import display_menu
from src.game.players import inp_player
from src.utils.find_game import find_game

from src.interprete.narrador import Locutor


class Game(object):
    def __init__(self):
        self.db = JsonDB()
        self.turno = 1

    def start(self):
        """
        Metodo de arranque del juego
        :return:
        """
        start_menu = {
            1: {'INICIAR JUEGO': self.game},
            # 'HISTORIAL'
            # 'BORRAR DB'
            2: {'SALIR': self.end_game}
        }
        func = display_menu(start_menu)
        func()

    def game(self):
        """
        Función que administra la lógica del juego.
        Esta función es responsable de tomar la entrada del jugador,
        iniciar la pelea, luego incrementar el contador 'last_fight' y finalmente guardar el estado del juego.
        """
        # play1 = input(f'\nEscriba el nombre del jugador 1 (Default: {self.db.data["default"]["player1"]}): ')
        #
        # if play1 == '':
        #     inp_player(self.db.data['last_fight'], self.db.data['default']['player1'], 1)
        # else:
        #     inp_player(self.db.data['last_fight'], play1, 1)
        #
        # play2 = input(f'\nEscriba el nombre del jugador 2 (Default: {self.db.data["default"]["player2"]}): ')
        #
        # if play2 == '':
        #     inp_player(self.db.data['last_fight'], self.db.data['default']['player2'], 2)
        # else:
        #     inp_player(self.db.data['last_fight'], play2, 2)

        for jugador in [1, 2]:
            # Obtiene el nombre del jugador mediante la entrada y proporciona un nombre predeterminado en caso de que
            # no se proporciona ninguna entrada
            nombre_jugador = input(
                f'\nEscriba el nombre del jugador {jugador} (Default: {self.db.data["default"][f"player{jugador}"]}): ')

            # Verifica la entrada del nombre del jugador.
            # Si está vacía, se usa el nombre predeterminado, de lo
            # contrario, se usa la entrada proporcionada
            if not nombre_jugador:
                nombre_jugador = self.db.data['default'][f'player{jugador}']

            # Introduce los datos del jugador
            inp_player(self.db.data['last_fight'], nombre_jugador, jugador)

        self.start_fight(self.db.data['last_fight'])
        self.db.data['last_fight'] += 1
        self.db.save()

    def start_fight(self, fight_id):
        """
        Esta función es responsable de iniciar una pelea en un juego.

        Args:
        fight_id (int):Identificador de la pelea que desea iniciar.

        Return:

        """
        print('*' * 10, " Inicia el Combante ", '*' * 10)
        game_exists, game_index = find_game(self.db.data['games'], fight_id)

        if game_exists:
            # Extraer información del juego
            game_data = self.db.data['games'][game_index][str(fight_id)]['fight']
            player1, player2 = game_data['player1'], game_data['player2']
            health1, health2 = game_data['health1'], game_data['health2']

            # Lucha entre los dos jugadores
            fight_result = fight(fight_id, player1, player2, health1, health2)

            # Determinar ganador en funcion de la salud
            winner = player1 if fight_result["player1"] > fight_result["player2"] else player2
            print(f'LA PELEA HA TERMINADO. EL GANADOR ES {winner}')

            # Actualizar salud de los jugadores en la base de datos
            game_data['health1'], game_data['health2'] = fight_result['player1'], fight_result['player2']

            # Guardar cambios
            self.db.save()

            print('*' * 200)
            print(f'{"*" * 25} RESUMEN DE LA PELEA{"*" * 25}')
            sleep(2)
            print('*' * 200)

            # Narrar la pelea
            locutor = Locutor(players=self.db.data['games'][game_index][str(fight_id)]['data'])
            locutor.narrar()

    @staticmethod
    def end_game():
        """
        Método para terminar el juego

        Este método proporciona una representación visual del final del juego
        y cierra el programa con la salida 100.
        """
        print('GRACIAS POR JUGAR A TALANA KOMBAT')
        print("""░░░░░░░░▄██▄░░░░░░▄▄░░
░░░░░░░▐███▀░░░░░▄███▌
░░▄▀░░▄█▀▀░░░░░░░░▀██░
░█░░░██░░░░░░░░░░░░░░░
█▌░░▐██░░▄██▌░░▄▄▄░░░▄
██░░▐██▄░▀█▀░░░▀██░░▐▌
██▄░▐███▄▄░░▄▄▄░▀▀░▄██
▐███▄██████▄░▀░▄█████▌
▐████████████▀▀██████░
░▐████▀██████░░█████░░
░░░▀▀▀░░█████▌░████▀░░
░░░░░░░░░▀▀███░▀▀▀░░░░""")
        exit(100)
