def display_menu(options: dict or list):
    """
    Muestra un menú en la terminal y solicita al usuario que elija una opción.

    La funcion toma un diccionario o lista 'options', enumera y muestra las
    opciones disponibles, luego espera a que el usuario ingrese una opción válida.
    Si la opción elegida no es válida, le pide al usuario que intente nuevamente.
    De lo contrario, retorna la opción elegida.

    Argumentos:
    options (dict or list):Las opciones para mostrar.
    Cada opción debe ser un par clave-valor en el cual la clave es
    el nombre de la opción y el valor es la función correspondiente a esa opción.

    Retorno:
    La función correspondiente a la opción elegida por el usuario.
    """
    print("Menu:")
    for i, option in enumerate(options.values(), start=1):
        print(f"{i}. {list(option.keys())[0]}")

    while True:
        choice = input("\nElige una opción: ")
        if not choice.isdigit() or int(choice) < 1 or int(choice) > len(options):
            print("Opción invalida. Vuelva a intentar")
        else:
            return list(options[int(choice)].values())[0]
