# MOVIMIENTOS = ['A', 'S', 'D', 'W']
MOVIMIENTOS = {'A': 'retrocede', 'S': 'agacha', 'D': 'avanza', 'W': 'salta'}
GOLPES = ['P', 'K']

ATAQUES = {
    'default': {
        'P': {
            'point': 1,
            'name': 'Puño'
        },
        'K': {
            'point': 1,
            'name': 'Patada'
        }
    },
    'player1': {
        'DSDP': {
            'point': 3,
            'name': 'Taladoken'
        },
        'SDK': {
            'point': 2,
            'name': 'Remuyuken'
        }
    },
    'player2': {
        'ASAP': {
            'point': 3,
            'name': 'Taladoken'
        },
        'SAK': {
            'point': 2,
            'name': 'Remuyuken'
        }
    }
}
