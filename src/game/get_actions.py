from typing import Literal

from src.game.const import ATAQUES


def get_actions(player: str, combo: tuple[str, str], value: Literal['name', 'point'] = 'name') -> int or str:
    """
    Esta función recibe un jugador, una combinación de acciones y opcionalmente el valor de retorno deseado
    ('name' o 'point')
    Devuelve el valor (nombre o puntos) del primer ataque que encuentra en el combo, en el orden de jugador ≥ defecto.

    Args:
        player (str):  un nombre de jugador como string.
        combo (tuple):  una combinación de acciones en forma de tupla.
        value (Literal['name', 'point']):  un valor opcional que representa el tipo de retorno deseado
        ('name' o 'point')

    Returns:
        (int o str):   Si encuentra un ataque, devuelve el 'name' o 'point' del ataque, dependiendo de value;
                       caso contrario, devuelve 0.
    """
    # Creando listas para los ataques por defecto y los ataques del jugador
    default_attack = list(ATAQUES['default'].keys())
    player_attack = list(ATAQUES[player].keys())

    # Extrayendo movimiento y ataque del combo
    movimiento, attack = combo

    # Creando un string 'especial' a partir del movimiento y el ataque
    especial = f'{movimiento}{attack if attack is not None else ""}'

    # Revisando los ataques en la lista del jugador
    for action in player_attack:
        index = especial.find(action)
        if index != -1:
            attack = ATAQUES[player][action][value]
            return attack

    # Revisando los ataques en la lista por defecto
    for action in default_attack:
        index = especial.find(action)
        if index != -1:
            attack = ATAQUES['default'][action][value]
            return attack

    return 0
