from src.db.controller import JsonDB
from src.utils.find_game import find_game


def inp_player(fight: int = None, player_name: str = None, player_id: int = None) -> None:
    """
    Esta función maneja la inicialización y asignación de valores para los jugadores y sus datos en la base de datos.

    Parámetros:
        fight (int): es el identificador del juego.
        player_name (str): nombre del jugador que se está ingresando.
        player_id (int): identificador del jugador que se está ingresando.

    """
    db = JsonDB()

    # Asegura que los datos de entrada son correctos
    assert fight is not None and isinstance(fight, int)
    assert player_name is not None and isinstance(player_name, str)
    assert player_id is not None and isinstance(player_id, int)

    # Busca el juego en la base de datos
    find, index = find_game(db.data['games'], fight)

    # Si el juego ya existe en la base de datos, actualizamos los datos del jugador
    if find:
        db.data['games'][index][str(fight)]['fight'][f'player{player_id}'] = player_name
        db.data['games'][index][str(fight)]['fight'][f'health{player_id}'] = 6
        # Guarda los cambios en la base de datos
        db.save()
    else:
        # Si el juego no existe en la base de datos, lo creamos y asignamos los datos del jugador
        db.data['games'].append(
            {
                f"{fight}": {
                    "fight": {
                        f"player{player_id}": player_name,
                        f"health{player_id}": 6,
                    }
                }
            }
        )
        db.save()
