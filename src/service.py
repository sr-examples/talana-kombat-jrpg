from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from src.config.settings import BASE_DIR
from src.models.entity_model import Game
from src.interprete.narrador import Locutor

# Crear la aplicación FastAPI
app = FastAPI()
app.mount('/static', StaticFiles(directory='static'), name='static')

# Configurar Jinja2 Templates
templates = Jinja2Templates(directory=str(BASE_DIR / "templates"))


@app.get("/")
def home():
    """
    Ruta principal ("/") que responde a los métodos GET de HTTP.
    Retorna un diccionario con un mensaje indicando que el servidor está funcionando.

    :return: Un diccionario con el estado del servidor.
    :rtype: Dict
    """
    return dict(Server="running")


@app.get("/interpreter")
async def get_interpreter(request: Request):
    """
    Ruta para la interfaz de intérprete.

    :param request: Objeto de solicitud HTTP.
    :return: Respuesta de la plantilla 'interprete.html'.
    """
    return templates.TemplateResponse(
        "interpreter.html", {"request": request}
    )


@app.post('/interpreter')
async def post_interpreter(body: Game):
    """
    Ruta para procesar las solicitudes POST del intérprete.

    :param body: Datos del juego proporcionados en el cuerpo de la solicitud.
    :return: El guion generado por el locutor.
    """
    locutor = Locutor(players=body.to_dict())
    locutor.crear_guion()
    return locutor.guion
