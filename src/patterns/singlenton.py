class SingletonMeta(type):
    """
    Metaclase para implementar el patrón de diseño Singleton.

    Esta metaclase garantiza que solo se pueda crear una única
    instancia de la clase de la cual se derivará.
    """
    _instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance
