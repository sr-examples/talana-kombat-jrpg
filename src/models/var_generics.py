from enum import Enum
from typing import TypeVar

# Declaramos T como un tipo genérico.
T = TypeVar("T")

# Declaramos EnumT como un tipo genérico que se limita a cualquier subclase de Enum
EnumT = TypeVar("EnumT", bound=Enum)
