from pydantic import BaseModel
from typing import Optional, List


class Player(BaseModel):
    """
    Esta es una clase que representa a un jugador en un juego.

    Parámetros
    ----------
    movimientos : Optional[List[str]]
        Los movimientos realizados por el jugador. Este argumento es opcional.
    golpes : Optional[List[Golpe]]
        Los golpes dados por el jugador. Este argumento es opcional.
    """
    name: Optional[str] = None
    movimientos: Optional[List[str]] = None
    golpes: Optional[List[str]] = None


# ===================================================================================================================

class Game(BaseModel):
    """
    Esta clase representa un juego.
    Atributos:
    ----------
    player1: Player, opcional.El primer jugador en el juego.
    player2: Player, opcional.El segundo jugador en el juego.
    """
    player1: Optional[Player] = None
    player2: Optional[Player] = None

    def to_dict(self) -> dict:
        return self.model_dump()


if __name__ == '__main__':
    game = Game(
        player1=Player(
            golpes=['S', 'K', ""]),
        player2=Player()
    )

    print(game.to_dict())
