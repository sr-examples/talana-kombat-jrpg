from src.game.start import Game


def run():
    """
    Función principal de la aplicación de consola que se encarga de la creación y el inicio del juego.

    Descripción:
        Crea una instancia del objeto Game e invoca al método start(), el cual
        inicializa y da inicio al flujo principal del juego.

    Nota:Esta función no acepta argumentos ni devuelve resultados.
    """
    game = Game()
    game.start()
