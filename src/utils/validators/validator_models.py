from typing import Callable, Any, List, Type

from src.models.var_generics import T, EnumT


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y.upper()) if isinstance(y, str) else f(y) for y in x]


def from_str(x: Any) -> str:
    """
    Esta función verifica si el valor de entrada es una cadena y luego lo retorna.

    Parámetros
    ----------
    x : Any
        Un valor que será verificado por su tipo de cadena.

    Devuelve
    -------
    str
        Devuelve el valor de entrada si es una cadena.

    Excepciones
    ------
    AssertionError
        Si el valor de entrada no es del tipo cadena.

    Ejemplos
    --------
    >>> from_str("Hola Mundo")
    'Hola Mundo'

    Notas
    -----
    Esta función generalmente se utiliza para asegurar que el valor de entrada dado es de hecho una cadena.
    Si no es una cadena, la afirmación fallará y se lanzará una excepción.
    """
    assert isinstance(x, str)
    return x


def from_none(x: Any) -> Any:
    """
    Esta función verifica si el valor ingresado es None y lo retorna sin cambios.

    Parámetros
    ----------
    x : Any
        Un valor que se verificará si es None.

    Devuelve
    -------
    Any
        Devuelve el valor ingresado sin cambios.

    Excepciones
    ------
    AssertionError
        Si el valor ingresado no es None.

    Ejemplos
    --------
    >>> from_none(None)
    None

    Notas
    -----
    Esta función generalmente se utiliza en el análisis y la manipulación de estructuras de datos donde
    puede haber valores nulos o None, asegurándose de que se verifica y se maneja correctamente el None.
    """
    assert x is None
    return x


def from_union(fs, x):
    """
    Esta función intenta aplicar una lista de funciones a un valor dado hasta que una no genere una excepción.

    Parámetros
    ----------
    fs : list
        Una lista de funciones que toman un argumento y devuelven un valor.
    x : any
        Un valor al que se aplicarán las funciones.

    Devuelve
    -------
    any
        El valor de retorno de la primera función que no genera una excepción al aplicarse a `x`.

    Excepciones
    ------
    AssertionError
        Si todas las funciones en `fs` generan una excepción al aplicarse a `x`.

    Ejemplos
    --------
    >>> def incremento(i): return i + 1
    >>> def decremento(y): return y - 1
    >>> from_union([incremento, decremento], 3)
    4

    Notas
    -----
    Esta función se usa normalmente con una lista de funciones de análisis sintáctico, donde cada una intenta analizar
    `x` de una forma diferente.
    La idea es que `x` debe poder ser analizado por al menos una de las funciones de análisis sintáctico proporcionadas.
    """
    for f in fs:
        try:
            return f(x)
        except TypeError as e:
            print(e)
            raise ValueError('Error in function {}'.format(f))
    assert False


def to_enum(c: Type[EnumT], x: Any) -> Callable[[], Any] | None:
    """
    Esta función devuelve el valor asociado con un miembro de un enumerado.

    Parámetros
    ----------
    c : Type[EnumT]
        El tipo del enumerado del cual es miembro 'x'.
    x : Any
        Un miembro de un enumerado del cual se quiere obtener su valor.

    Devuelve
    -------
    Callable[[], Any]
        El valor asociado con 'x' en el enumerado.

    Excepciones
    ------
    AssertionError
        Si 'x' no es un miembro del enumerado 'c'.

    Ejemplos
    --------
    >>> from enum import Enum
    >>> class Color(Enum):
    ...     ROJO = 1
    ...     VERDE = 2
    ...     AZUL = 3
    >>> to_enum(Color, Color.ROJO)
    1

    Notas
    -----
    Esta función es útil en situaciones en las que se tienen enumerados y se necesita
    obtener el valor asociado con un miembro del enumerado.
    """
    if isinstance(x, c):
        return x.value
    return None
