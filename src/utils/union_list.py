def union_list(principal: list, secundaria: list | str, formato=True):
    """
    Une dos listas en una sola, elemento a elemento.

    :param principal: La primera lista.
    :param secundaria: La segunda lista.
    :param formato: Un indicador para aplicar un formato específico a la salida.
                    Si es verdadero, se usa una fórmula de formato. Si es falso, se concatenan los elementos
                    directamente.
    :return: Una lista con los elementos de ambas listas unidos.

    Se asume que ambas listas tienen la misma longitud.
    """
    # Se asegura que las dos listas tienen la misma longitud.
    assert len(principal) == len(secundaria)

    # Dependiendo del valor de 'format', se aplicará una fórmula de formato a cada par de elementos o no.
    if formato:
        return [f'{p} ({s})' for p, s in zip(principal, secundaria)]
    else:
        return [f'{p}{s if s is not None else ""}' for p, s in zip(principal, secundaria)]
