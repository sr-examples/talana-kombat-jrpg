import keyboard

from src.utils.union_list import union_list


def capture_keypress(player, motion_characters: list = None, finish_characters: list = None, max_characters=5,
                     instructions: bool = False) -> tuple[str, str]:
    assert motion_characters is not None and finish_characters is not None

    characters_accept = motion_characters + finish_characters

    print('*' * 200)

    print(f'>> Turno de Jugador {player}')

    if instructions:
        print('\n-- INTRUCCIONES --')
        print('Puedes utilizar estas teclas para moverte',
              union_list(motion_characters, '🡠🡣🡢🡡'),
              'y puedes utilizar',
              union_list(finish_characters, ['Puño', 'Patada']), 'para atacar')
        print('Tienes un maximo de 5 movimientos y 1 solo ataque')
        print('\nPuedes finalizar el turno presionando la tecla "Enter" o movimiento de ataque')

    movimiento = ''
    golpe = ''

    print('Ingrese su movimiento:')
    while True:
        event = keyboard.read_event(suppress=True)
        if event.event_type == keyboard.KEY_DOWN:
            key = event.name.upper()
            if key in finish_characters:
                golpe += key
                print('Jugada:', movimiento + golpe)
                break
            elif key.isalpha() and len(movimiento) <= max_characters and key in characters_accept:
                movimiento += key
                print('Jugada:', movimiento + golpe)
            elif event.name == "enter":
                break
            elif event.name == "backspace":
                movimiento = movimiento[:-1]

    return movimiento, golpe
