def find_game(games: list, fight) -> tuple[bool, int or None]:
    for i, game in enumerate(games):
        if str(fight) in game:
            return True, i
    return False, None
