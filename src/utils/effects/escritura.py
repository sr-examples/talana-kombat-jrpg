from sys import stdout
from time import sleep


def mecanografiar(text: str, time: float = 0.200):
    lista = ('>> ' + text).split()

    for palabra in lista:
        stdout.write(palabra + " ")
        stdout.flush()
        sleep(time)


def escribir_guion(guion: list[str], time: float = 0.200):
    for linea in guion:
        mecanografiar(linea, time)
        print()
